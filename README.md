# Delegate Activity in C#

## Objective

This activity aims to teach the use of both individual and multicast delegates in C#. Learners will understand how to define and use delegates, the difference between individual and multicast delegates, and the significance of the `+=` operator in chaining multiple methods to a single delegate instance.

## Instructions

1. **Setting Up the Environment**:
    - Create a new C# project in your preferred IDE.
    - Start by defining a delegate called `AstronomicalEventHandler` that takes a single string parameter.

2. **Creating the ObservatoryControl Class**:
    - In your project, create a new class named `ObservatoryControl`.
    - Inside `ObservatoryControl`, define two methods: `HandleSolarFlare` and `HandleSupernova`. Each method should take a string parameter and output a message to the console indicating that it is handling a solar flare or a supernova, respectively.

3. **Implementing Separate Delegates**:
    - In the `Program` class, create an instance of `ObservatoryControl`.
    - Define two delegate instances, `solarFlareHandler` and `supernovaHandler`, of type `AstronomicalEventHandler`.
    - Assign the `HandleSolarFlare` method to `solarFlareHandler` and `HandleSupernova` to `supernovaHandler`.

4. **Simulating Astronomical Events**:
    - Create two string variables, `receivedEvent1` and `receivedEvent2`, each representing a different type of astronomical event (solar flare and supernova).
    - Write conditional statements to check the content of each received event. If the event contains "Solar Flare", invoke `solarFlareHandler`, and if it contains "Supernova", invoke `supernovaHandler`.

5. **Demonstrating Multicast Delegates**:
    - Introduce the concept of multicast delegates.
    - Create a delegate instance `eventHandler` of type `AstronomicalEventHandler` and set it to `null`.
    - Use the `+=` operator to chain `HandleSolarFlare` and `HandleSupernova` methods to `eventHandler`.
    - Invoke `eventHandler` with the same event messages as before.

6. **Observing the Output**:
    - Run your application and observe the output.
    - Notice how the separate delegate approach handles each event specifically, while the multicast delegate approach triggers multiple handlers from a single event invocation.

## Expected Learning Outcomes

Through this activity, learners should understand the use of delegates in C# and how multicast delegates can trigger multiple methods simultaneously, emphasizing the flexibility and modular nature of event-driven programming in C#.
