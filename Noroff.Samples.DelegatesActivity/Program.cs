﻿namespace Noroff.Samples.DelegatesActivity
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Creating our ObservatoryHandler
            ObservatoryControl control = new ObservatoryControl();
            // Define separate delegates for different event types
            Console.WriteLine("\nSeparate Delegates Approach:");
            AstronomicalEventHandler solarFlareHandler = control.HandleSolarFlare;
            AstronomicalEventHandler supernovaHandler = control.HandleSupernova;

            // Simulate receiving different types of astronomical events
            string receivedEvent1 = "Solar Flare: Intense radiation storm approaching Earth";
            string receivedEvent2 = "Supernova: Bright star explosion observed in distant galaxy";

            // Invoke the corresponding event handler for each event
            if (receivedEvent1.Contains("Solar Flare"))
            {
                solarFlareHandler?.Invoke(receivedEvent1);
            }

            if (receivedEvent2.Contains("Supernova"))
            {
                supernovaHandler?.Invoke(receivedEvent2);
            }

            // Using a multicast delegate - multiple events triggered from one call with += chain
            AstronomicalEventHandler eventHandler = null;
            eventHandler += control.HandleSolarFlare;
            eventHandler += control.HandleSupernova;

            Console.WriteLine("Multicast Delegate Approach:");
            eventHandler?.Invoke("Solar Flare: Intense radiation storm approaching Earth");
            eventHandler?.Invoke("Supernova: Bright star explosion observed in distant galaxy");
        }
    }

    public delegate void AstronomicalEventHandler(string eventDescription);

    // This class contains implementations for the event handler defined by the delegate above.
    public class ObservatoryControl
    {
        public void HandleSolarFlare(string message)
        {
            Console.WriteLine($"Handling solar flare: {message}");
        }

        public void HandleSupernova(string message)
        {
            Console.WriteLine($"Handling supernova: {message}");
        }
    }
}